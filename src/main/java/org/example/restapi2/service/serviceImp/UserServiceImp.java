package org.example.restapi2.service.serviceImp;

import org.example.restapi2.model.dto.UserDto;
import org.example.restapi2.model.entity.UserApp;
import org.example.restapi2.model.mapper.UserMapper;
import org.example.restapi2.repository.UserRepository;
import org.example.restapi2.service.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImp implements UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserServiceImp(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    UserApp userApp = new UserApp();
    UserDto userDto = new UserDto();

    @Override
    public UserDto getAllUserById(Integer id) {
        UserApp userApp = userRepository.getAllUserById(id);
//        userDto.setId(userApp.getId());
//        userDto.setName(userApp.getName());
//        userDto.setEmail(userApp.getEmail());
        userDto = userMapper.toUserDto(userApp);
        return userDto;
    }

    @Override
    public List<UserDto> getAllUsers() {
        List<UserApp> userApps = new ArrayList<>();
        List<UserDto> userDtos = new ArrayList<>();

        userApps = userRepository.getAllUsers();
        List<UserDto> UserDto = userMapper.toUserDtos(userApps);
        return UserDto;
    }

}
