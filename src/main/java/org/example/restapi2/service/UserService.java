package org.example.restapi2.service;

import org.example.restapi2.model.dto.UserDto;

import java.util.List;


public interface UserService  {
    UserDto getAllUserById(Integer id);

    List<UserDto> getAllUsers();
}
