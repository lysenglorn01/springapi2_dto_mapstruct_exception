package org.example.restapi2.controller;

import org.example.restapi2.model.dto.UserDto;
import org.example.restapi2.model.response.ApiBaseResponse;
import org.example.restapi2.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<?> getAllUserById(@PathVariable Integer id) {
        UserDto userDto = userService.getAllUserById(id);
        ApiBaseResponse<UserDto> response = ApiBaseResponse.<UserDto>builder()
                .time(LocalDateTime.now())
                .message("Successful fetch data")
                .status(HttpStatus.OK)
                .data(userDto)
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/users")
    public ResponseEntity<?> getAllUsers() {
        List<UserDto> userDto = userService.getAllUsers();
        ApiBaseResponse<List<UserDto>> response = ApiBaseResponse.<List<UserDto>>builder()
                .time(LocalDateTime.now())
                .status(HttpStatus.OK)
                .message("Successful fetch all users")
                .data(userDto)
                .build();
        return ResponseEntity.ok(response);
    }

}
