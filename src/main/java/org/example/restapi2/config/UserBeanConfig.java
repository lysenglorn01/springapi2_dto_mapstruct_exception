package org.example.restapi2.config;

import org.example.restapi2.model.mapper.UserMapper;
import org.example.restapi2.model.mapper.UserMapperImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserBeanConfig {
    @Bean
    public UserMapper userMapper(){
        return new UserMapperImpl();
    }
}
