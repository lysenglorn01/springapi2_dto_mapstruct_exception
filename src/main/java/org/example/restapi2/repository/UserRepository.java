package org.example.restapi2.repository;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.example.restapi2.model.entity.UserApp;

import java.util.List;

@Mapper
public interface UserRepository {
    @Select("""
            SELECT * FROM users
            WHERE id = #{id}
            """)
    UserApp getAllUserById(Integer id);

    @Select("""
            SELECT * FROM users
            """)
    List<UserApp> getAllUsers();
}
