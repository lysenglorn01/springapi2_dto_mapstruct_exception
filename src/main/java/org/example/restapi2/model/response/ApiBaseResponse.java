package org.example.restapi2.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ApiBaseResponse<T> {
    private LocalDateTime time;
    private HttpStatus status;
    private String message;
    private T data;
}
