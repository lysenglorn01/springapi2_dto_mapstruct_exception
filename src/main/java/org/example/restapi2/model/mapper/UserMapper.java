package org.example.restapi2.model.mapper;

import org.example.restapi2.model.dto.UserDto;
import org.example.restapi2.model.entity.UserApp;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {
    UserDto toUserDto (UserApp UserApp);

    List<UserDto> toUserDtos (List<UserApp> userApps);
}
